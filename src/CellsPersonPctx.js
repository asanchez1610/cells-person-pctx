import { html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsPersonPctx-styles.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements/BaseElement';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-person-pctx></cells-person-pctx>
```

##styling-doc

@customElement cells-person-pctx
*/
export class CellsPersonPctx extends BaseElement {
  static get is() {
    return 'cells-person-pctx';
  }

  // Declare properties
  static get properties() {
    return {
      persona: Object
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-person-pctx-shared-styles')
    ];
  }

  registrar() {
    const inputs = this.elementsAll('.input-form');
    this.persona = {};
    let temp = {};
    inputs.forEach(item => {
      temp[item.name] = item.value;
    });
    this.persona = temp;
    this.dispatch('on-register-person', this.persona);
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <main>
        <bbva-web-form-text class="input-form" name="nombres" label="Nombres" ></bbva-web-form-text>
        <bbva-web-form-text class="input-form" name="apellidos" label="Apellidos" ></bbva-web-form-text>

        <bbva-web-form-select class="input-form" name="tipoDocumento" label="Tipo de documento">
          <bbva-web-form-option value="DNI">DNI</bbva-web-form-option>
          <bbva-web-form-option value="RUC">RUC</bbva-web-form-option>
          <bbva-web-form-option value="CE">Carnet de Extranjeria</bbva-web-form-option>
        </bbva-web-form-select>

        <bbva-web-form-text class="input-form" name="numDocumento" label="Número de documento" ></bbva-web-form-text>

        <bbva-web-button-default @click="${this.registrar}" >Registrar</bbva-web-button-default>
      </main>
    `;
  }
}
